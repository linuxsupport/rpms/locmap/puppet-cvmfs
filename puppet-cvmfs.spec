Name:		puppet-cvmfs
Version:	2.10
Release:	1%{?dist}
Summary:	Puppet module for cvmfs

Group:		CERN/Utilities
License:	BSD
URL:		http://linux.cern.ch
Source0:	%{name}-%{version}.tgz

BuildArch: noarch

Requires: puppet-stdlib
Requires: puppet-concat
Requires: puppet-agent

%description
Puppet module for cvmfs.

%prep
%setup -q

%build
FLAGS="%{optflags}"

%install

rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/cvmfs
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/cvmfs/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/cvmfs/operatingsystems/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/cvmfs/operatingsystems/RedHat/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/cvmfs/operatingsystems/CentOS/

cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/cvmfs/
touch %{buildroot}/%{_datadir}/puppet/modules/cvmfs/linuxsupport

cp data/cvmfs.yaml %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/cvmfs/cvmfs.yaml


%files -n puppet-cvmfs
/etc/locmap/code/environments/production/hieradata/modules/cvmfs/cvmfs.yaml
%{_datadir}/puppet/modules/cvmfs
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 15 2024 Ben Morrice <ben.morrice@cern.ch> 2.10-1
- Patched autofs resource is no longer required

* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.9-1
- Add autoreconfigure %post script

* Wed Oct 04 2023 Ben Morrice <ben.morrice@cern.ch> 2.8-1
- Rebase to #5d6259d4c13f044b0d9de1174d450f551ac4c455

* Tue Sep 26 2023 Manuel Guijarro <manuel.guijarro@cern.ch> 2.7-3
- Fixed includepkgs for cvmfs

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.7-2
- Bump release for disttag change

* Tue Jul 12 2022 Ben Morrice <ben.morrice@cern.ch> - 2.7-1
- promote production release on el9

* Fri Apr 29 2022 Ben Morrice <ben.morrice@cern.ch> - 2.6-1
- deploy testing release on el9

* Thu Jan 27 2022 Ben Morrice <ben.morrice@cern.ch> - 2.5-1
- add requires on cvmfs package to avoid failure of cvmfsfacts.yaml

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.4-3
- fix requires on puppet-agent

* Mon Feb 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.4-2
- patch puppet code to avoid duplicate resource definitions

* Fri Jan 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.4-1
- Rebase to version 7.3.0

* Mon Feb 17 2020 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
 - Rebase to version 7.0.1

* Wed Jan 08 2020 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
 - Rebase to version 7.0.0

* Mon Nov 26 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.1-1
- Rebase on commit #ac932504ba97dbeaa59282c8aedfc73e5d7963e0

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-4
- Rebuild for 7.5
- Rebase on latest module

* Thu Dec 15 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-4
- Fix default boolean values in .yaml

* Mon Dec 12 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-2
- Fix yumrepo creation

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Mon Sep 19 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Release 3.1.0 version

* Fri Aug 26 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Release 3.0.1 version

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
